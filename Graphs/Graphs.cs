﻿using System.Windows.Forms;

namespace Graphs
{
    public partial class Graphs : Form
    {
        private Graph _graph;
        private string _code;

        public RenderMode RenderMode = RenderMode.Original;
        public int StartVertex = 0;
        public int CountOfVertices = 0;
        public int DelayTime = 0;

        public Graphs(string graphCode)
        {
            InitializeComponent();
            InitGraph(graphCode);
        }

        private void InitGraph(string graphCode)
        {
            _graph = new Graph(this);
            _graph.ParseCode(graphCode);
            _code = graphCode;
            CountOfVertices = _graph.GetCountOfVertices();
            Paint += Graphs_Paint;

            TestMsg.Text = _graph.ToString();
            Show();
        }

        private void Graphs_Paint(object sender, PaintEventArgs e) { _graph.RewrawLines(e, RenderMode, StartVertex, DelayTime); }

        private void Graphs_FormClosed(object sender, FormClosedEventArgs e)
        {
            _graph.Dispose();
            _graph = null;
            ((MainForm) Owner).GraphFormDispose();
        }

        public FirstSearchResult Dfs(string searchString, int root = 1)
        {
            return _graph.Dfs(searchString, root);
        }

        public FirstSearchResult Bfs(string searchString, int root = 1)
        {
            return _graph.Bfs(searchString, root);
        }

        public FirstSearchResult Dijkstra(string searchString, int root = 1)
        {
            return _graph.Dijkstra(searchString, root);
        }

        public void Reset()
        {
            _graph.Dispose();
            _graph = new Graph(this);
            _graph.ParseCode(_code);
            CountOfVertices = _graph.GetCountOfVertices();
            TestMsg.Text = _graph.ToString();
            Refresh();
        }
    }
}
