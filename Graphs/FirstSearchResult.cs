﻿namespace Graphs
{
    public class FirstSearchResult
    {
        private readonly int _vertexNumber;
        private readonly string _vertexContent;
        private readonly int _steps;
        private readonly int _distance;

        public FirstSearchResult(int vn, string vc, int s, int d)
        {
            _vertexNumber = vn;
            _vertexContent = vc;
            _steps = s;
            _distance = d;
        }

        public int GetVertexNumber() { return _vertexNumber; }
        public string GetVertexContent() { return _vertexContent; }
        public int GetSteps() { return _steps; }
        public int GetDistance() { return _distance; }
    }
}
