﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Graphs
{
    internal class Vertex
    {
        private List<Vertex> _adjacentVertices = new List<Vertex>();
        private List<int> _distances = new List<int>();
        private string _content;
        private VertexMark _mark = VertexMark.White;

        private int _id;
        private static int _counter = -1;

        // Keeps any additional information
        public object Tag = null;

        public int DijkstraCost = 999999;
        public bool DijkstraBlocked = false;

        public Vertex(string content)
        {
            _id = ++_counter;
            _content = content;
        }

        public override string ToString()
        {
            var resultStr = "[Vertex: " + _content + "]: {\n";
            for (var i = 0; i < _adjacentVertices.Count; i++)
                resultStr += "      Vertex: " + _adjacentVertices[i]._content + ", distance: " + _distances[i] + "\n";
            return resultStr + "   }";
        }

        public string GetContent() => _content;
        public int GetId() => _id;

        public void AddAdjacentVertex(Vertex adjacentVertex, int distance)
        {
            _adjacentVertices.Add(adjacentVertex);
            _distances.Add(distance);
        }

        public Vertex GetWhiteVertex()
        {
            return _adjacentVertices.FirstOrDefault(vertex => vertex._mark == VertexMark.White);
        }

        public Vertex GetNonBlackVertex()
        {
            return _adjacentVertices.FirstOrDefault(vertex => (vertex._mark != VertexMark.Black) && !vertex.DijkstraBlocked);
        }

        public Vertex GetNonGrayVertex()
        {
            return _adjacentVertices.FirstOrDefault(vertex => vertex._mark != VertexMark.Gray);
        }

        public FirstSearchResult Dfs(string whatLookingFor)
        {
            _mark = VertexMark.Gray;
            if (_content == whatLookingFor)
                return new FirstSearchResult(_id, _content, Graph.Steps, Graph.Distance);

            Vertex vertex;
            while ((vertex = GetWhiteVertex()) != null)
            {
                Graph.Steps++;
                var i = 0;
                for (; i < _adjacentVertices.Count; i++)
                {
                    if (_adjacentVertices[i] == vertex)
                    {
                        Graph.Distance += _distances[i];
                        break;
                    }
                }

                var res = vertex.Dfs(whatLookingFor);
                if (res != null) return res;

                Graph.Steps++;
                Graph.Distance += _distances[i];
            }

            _mark = VertexMark.Black;
            return null;
        }

        public void DFS_Animation(PaintEventArgs e, GraphRenderer renderer, int sleeptime = 100)
        {
            _mark = VertexMark.Gray;
            var lastPoint = renderer.Label_GetLocation(_id);
            System.Threading.Thread.Sleep(sleeptime);

            Vertex vertex;
            while ((vertex = GetWhiteVertex()) != null)
            {
                e.Graphics.DrawLine(renderer.GetPen(), lastPoint, renderer.Label_GetLocation(vertex.GetId()));
                var tmpPoint = lastPoint;
                vertex.DFS_Animation(e, renderer, sleeptime);
                lastPoint = tmpPoint;
            }

            _mark = VertexMark.Black;
            renderer.Label_ChangeColor(_id, Color.Black, Color.White);
        }

        public void ForEach(PaintEventArgs e, GraphRenderer renderer)
        {
            foreach (var vertex in _adjacentVertices)
                e.Graphics.DrawLine(renderer.GetPen(), renderer.Label_GetLocation(GetId()), renderer.Label_GetLocation(vertex.GetId()));
        }

        public void MarkAs(VertexMark color) { _mark = color; }

        public int GetDistanceBetween(Vertex secondVertex)
        {
            for (var i = 0; i < _adjacentVertices.Count; i++)
            {
                var av = _adjacentVertices[i];
                if (av == secondVertex)
                    return _distances[i];
            }

            return 0;
        }

        public void Reset()
        {
            _mark = VertexMark.White;
        }

        public void Dispose()
        {
            _adjacentVertices = null;
            _distances = null;
            _content = null;
            _id = 0;
            _counter = -1;
        }
    }

    internal enum VertexMark { White, Gray, Black };
}
