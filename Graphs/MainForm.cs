﻿using System;
using System.Globalization;
using System.Windows.Forms;
using Graphs.Properties;

namespace Graphs
{
    public partial class MainForm : Form
    {
        private Graphs _graphForm;

        public MainForm()
        {
            InitializeComponent();
        }

        private void RunCodeBtn_Click(object sender, EventArgs e)
        {
            RunCodeBtn.Enabled = false;
            ResetBtn.Enabled = true;
            GroupOfManagement.Enabled = true;
            _graphForm = new Graphs(CodeTextArea.Text) { Owner = this };
            StartVertexUpDown.Maximum = WhatLookingForUpDown.Maximum = _graphForm.CountOfVertices;
        }

        private void ResetBtn_Click(object sender, EventArgs e)
        {
            _graphForm.Reset();
            OriginalRadioBtn.Checked = true;
            StartVertexUpDown.Value = 1;
            WhatLookingForUpDown.Value = 1;
            DelayTrackBar.Value = 1;
            CountOfSteps.Text = "0";
            Distance.Text = "0";
        }

        public void GraphFormDispose()
        {
            _graphForm.Dispose();
            _graphForm = null;

            RunCodeBtn.Enabled = true;
            ResetBtn.Enabled = false;
            GroupOfManagement.Enabled = false;
        }

        private void ShowHowItWorksBtn_Click(object sender, EventArgs e)
        {
            if (OriginalRadioBtn.Checked)
                _graphForm.RenderMode = RenderMode.Original;
            else if (DFSradioBtn.Checked)
                _graphForm.RenderMode = RenderMode.Dfs;
            else if (BFSradioBtn.Checked || DijkstraRadioBtn.Checked)
                _graphForm.RenderMode = RenderMode.Bfs;

            _graphForm.StartVertex = (int) StartVertexUpDown.Value - 1;
            _graphForm.DelayTime = DelayTrackBar.Value;
            _graphForm.Refresh();
            _graphForm.DelayTime = 0;
        }

        private void DFSradioBtn_CheckedChanged(object sender, EventArgs e)
        {
            DelayTrackBar.Enabled = !OriginalRadioBtn.Checked;
        }

        private void FindVertexBtn_Click(object sender, EventArgs e)
        {
            FirstSearchResult fsr = null;
            if (OriginalRadioBtn.Checked)
            {
                MessageBox.Show(Resources.UnsupportedOperationText, Resources.UnsupportedOperationTitle, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (DFSradioBtn.Checked)
                fsr = _graphForm.Dfs(WhatLookingForUpDown.Value.ToString(CultureInfo.CurrentCulture), (int) StartVertexUpDown.Value);
            else if (BFSradioBtn.Checked)
                fsr = _graphForm.Bfs(WhatLookingForUpDown.Value.ToString(CultureInfo.CurrentCulture), (int) StartVertexUpDown.Value);
            else if (DijkstraRadioBtn.Checked)
                fsr = _graphForm.Dijkstra(WhatLookingForUpDown.Value.ToString(CultureInfo.CurrentCulture), (int) StartVertexUpDown.Value);

            if (fsr != null)
            {
                CountOfSteps.Text = fsr.GetSteps().ToString();
                Distance.Text = fsr.GetDistance().ToString();
            }
            else MessageBox.Show(Resources.PathNotFoundText, Resources.ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }
    }
}
