﻿using Graphs.Properties;

namespace Graphs
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CodeTextArea = new System.Windows.Forms.RichTextBox();
            this.RunCodeBtn = new System.Windows.Forms.Button();
            this.GroupOfManagement = new System.Windows.Forms.GroupBox();
            this.DelayLabel = new System.Windows.Forms.Label();
            this.DelayTrackBar = new System.Windows.Forms.TrackBar();
            this.OriginalRadioBtn = new System.Windows.Forms.RadioButton();
            this.DijkstraRadioBtn = new System.Windows.Forms.RadioButton();
            this.Distance = new System.Windows.Forms.Label();
            this.CountOfSteps = new System.Windows.Forms.Label();
            this.DistanceLabel = new System.Windows.Forms.Label();
            this.CountOfStepsLabel = new System.Windows.Forms.Label();
            this.FindVertexBtn = new System.Windows.Forms.Button();
            this.WhatLookingForLabel = new System.Windows.Forms.Label();
            this.WhatLookingForUpDown = new System.Windows.Forms.NumericUpDown();
            this.SetStartVertexLabel = new System.Windows.Forms.Label();
            this.StartVertexUpDown = new System.Windows.Forms.NumericUpDown();
            this.BFSradioBtn = new System.Windows.Forms.RadioButton();
            this.DFSradioBtn = new System.Windows.Forms.RadioButton();
            this.ShowHowItWorksBtn = new System.Windows.Forms.Button();
            this.ResetBtn = new System.Windows.Forms.Button();
            this.GroupOfManagement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DelayTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhatLookingForUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartVertexUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // CodeTextArea
            // 
            this.CodeTextArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CodeTextArea.Location = new System.Drawing.Point(10, 12);
            this.CodeTextArea.Name = "CodeTextArea";
            this.CodeTextArea.Size = new System.Drawing.Size(127, 234);
            this.CodeTextArea.TabIndex = 1;
            this.CodeTextArea.Text = "AddVertex(5);\nAddVertex(3);\nBack();\nAddVertex(2);\nJoinVertex(1, 3);\nAddVertex(2);" +
    "\nAddVertex(1);\nJoinVertex(2, 7);\nAddVertex(1);\nJoinVertex(4, 1);\nClose(3);";
            // 
            // RunCodeBtn
            // 
            this.RunCodeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RunCodeBtn.Location = new System.Drawing.Point(10, 257);
            this.RunCodeBtn.Name = "RunCodeBtn";
            this.RunCodeBtn.Size = new System.Drawing.Size(127, 23);
            this.RunCodeBtn.TabIndex = 2;
            this.RunCodeBtn.Text = "Построить";
            this.RunCodeBtn.UseVisualStyleBackColor = true;
            this.RunCodeBtn.Click += new System.EventHandler(this.RunCodeBtn_Click);
            // 
            // GroupOfManagement
            // 
            this.GroupOfManagement.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupOfManagement.Controls.Add(this.DelayLabel);
            this.GroupOfManagement.Controls.Add(this.DelayTrackBar);
            this.GroupOfManagement.Controls.Add(this.OriginalRadioBtn);
            this.GroupOfManagement.Controls.Add(this.DijkstraRadioBtn);
            this.GroupOfManagement.Controls.Add(this.Distance);
            this.GroupOfManagement.Controls.Add(this.CountOfSteps);
            this.GroupOfManagement.Controls.Add(this.DistanceLabel);
            this.GroupOfManagement.Controls.Add(this.CountOfStepsLabel);
            this.GroupOfManagement.Controls.Add(this.FindVertexBtn);
            this.GroupOfManagement.Controls.Add(this.WhatLookingForLabel);
            this.GroupOfManagement.Controls.Add(this.WhatLookingForUpDown);
            this.GroupOfManagement.Controls.Add(this.SetStartVertexLabel);
            this.GroupOfManagement.Controls.Add(this.StartVertexUpDown);
            this.GroupOfManagement.Controls.Add(this.BFSradioBtn);
            this.GroupOfManagement.Controls.Add(this.DFSradioBtn);
            this.GroupOfManagement.Controls.Add(this.ShowHowItWorksBtn);
            this.GroupOfManagement.Enabled = false;
            this.GroupOfManagement.Location = new System.Drawing.Point(149, 10);
            this.GroupOfManagement.Name = "GroupOfManagement";
            this.GroupOfManagement.Size = new System.Drawing.Size(184, 302);
            this.GroupOfManagement.TabIndex = 3;
            this.GroupOfManagement.TabStop = false;
            this.GroupOfManagement.Text = "Обходы";
            // 
            // DelayLabel
            // 
            this.DelayLabel.AutoSize = true;
            this.DelayLabel.Location = new System.Drawing.Point(119, 45);
            this.DelayLabel.Name = "DelayLabel";
            this.DelayLabel.Size = new System.Drawing.Size(61, 13);
            this.DelayLabel.TabIndex = 17;
            this.DelayLabel.Text = "Задержка:";
            // 
            // DelayTrackBar
            // 
            this.DelayTrackBar.Enabled = false;
            this.DelayTrackBar.Location = new System.Drawing.Point(123, 58);
            this.DelayTrackBar.Maximum = 20;
            this.DelayTrackBar.Name = "DelayTrackBar";
            this.DelayTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.DelayTrackBar.Size = new System.Drawing.Size(45, 71);
            this.DelayTrackBar.TabIndex = 16;
            this.DelayTrackBar.Value = 1;
            // 
            // OriginalRadioBtn
            // 
            this.OriginalRadioBtn.AutoSize = true;
            this.OriginalRadioBtn.Checked = true;
            this.OriginalRadioBtn.Location = new System.Drawing.Point(23, 43);
            this.OriginalRadioBtn.Name = "OriginalRadioBtn";
            this.OriginalRadioBtn.Size = new System.Drawing.Size(60, 17);
            this.OriginalRadioBtn.TabIndex = 15;
            this.OriginalRadioBtn.TabStop = true;
            this.OriginalRadioBtn.Text = "Original";
            this.OriginalRadioBtn.UseVisualStyleBackColor = true;
            this.OriginalRadioBtn.CheckedChanged += new System.EventHandler(this.DFSradioBtn_CheckedChanged);
            // 
            // DijkstraRadioBtn
            // 
            this.DijkstraRadioBtn.AutoSize = true;
            this.DijkstraRadioBtn.Location = new System.Drawing.Point(23, 104);
            this.DijkstraRadioBtn.Name = "DijkstraRadioBtn";
            this.DijkstraRadioBtn.Size = new System.Drawing.Size(67, 17);
            this.DijkstraRadioBtn.TabIndex = 14;
            this.DijkstraRadioBtn.Text = "Dijkstra\'s";
            this.DijkstraRadioBtn.UseVisualStyleBackColor = true;
            this.DijkstraRadioBtn.CheckedChanged += new System.EventHandler(this.DFSradioBtn_CheckedChanged);
            // 
            // Distance
            // 
            this.Distance.AutoSize = true;
            this.Distance.Location = new System.Drawing.Point(91, 277);
            this.Distance.Name = "Distance";
            this.Distance.Size = new System.Drawing.Size(13, 13);
            this.Distance.TabIndex = 13;
            this.Distance.Text = "0";
            // 
            // CountOfSteps
            // 
            this.CountOfSteps.AutoSize = true;
            this.CountOfSteps.Location = new System.Drawing.Point(91, 254);
            this.CountOfSteps.Name = "CountOfSteps";
            this.CountOfSteps.Size = new System.Drawing.Size(13, 13);
            this.CountOfSteps.TabIndex = 12;
            this.CountOfSteps.Text = "0";
            // 
            // DistanceLabel
            // 
            this.DistanceLabel.AutoSize = true;
            this.DistanceLabel.Location = new System.Drawing.Point(13, 278);
            this.DistanceLabel.Name = "DistanceLabel";
            this.DistanceLabel.Size = new System.Drawing.Size(70, 13);
            this.DistanceLabel.TabIndex = 11;
            this.DistanceLabel.Text = "Расстояние:";
            // 
            // CountOfStepsLabel
            // 
            this.CountOfStepsLabel.AutoSize = true;
            this.CountOfStepsLabel.Location = new System.Drawing.Point(13, 254);
            this.CountOfStepsLabel.Name = "CountOfStepsLabel";
            this.CountOfStepsLabel.Size = new System.Drawing.Size(42, 13);
            this.CountOfStepsLabel.TabIndex = 10;
            this.CountOfStepsLabel.Text = "Шагов:";
            // 
            // FindVertexBtn
            // 
            this.FindVertexBtn.Location = new System.Drawing.Point(59, 212);
            this.FindVertexBtn.Name = "FindVertexBtn";
            this.FindVertexBtn.Size = new System.Drawing.Size(75, 23);
            this.FindVertexBtn.TabIndex = 9;
            this.FindVertexBtn.Text = "Найти";
            this.FindVertexBtn.UseVisualStyleBackColor = true;
            this.FindVertexBtn.Click += new System.EventHandler(this.FindVertexBtn_Click);
            // 
            // WhatLookingForLabel
            // 
            this.WhatLookingForLabel.AutoSize = true;
            this.WhatLookingForLabel.Location = new System.Drawing.Point(13, 185);
            this.WhatLookingForLabel.Name = "WhatLookingForLabel";
            this.WhatLookingForLabel.Size = new System.Drawing.Size(103, 13);
            this.WhatLookingForLabel.TabIndex = 8;
            this.WhatLookingForLabel.Text = "Искомая вершина:";
            // 
            // WhatLookingForUpDown
            // 
            this.WhatLookingForUpDown.Location = new System.Drawing.Point(122, 183);
            this.WhatLookingForUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.WhatLookingForUpDown.Name = "WhatLookingForUpDown";
            this.WhatLookingForUpDown.Size = new System.Drawing.Size(46, 20);
            this.WhatLookingForUpDown.TabIndex = 7;
            this.WhatLookingForUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // SetStartVertexLabel
            // 
            this.SetStartVertexLabel.AutoSize = true;
            this.SetStartVertexLabel.Location = new System.Drawing.Point(11, 22);
            this.SetStartVertexLabel.Name = "SetStartVertexLabel";
            this.SetStartVertexLabel.Size = new System.Drawing.Size(112, 13);
            this.SetStartVertexLabel.TabIndex = 5;
            this.SetStartVertexLabel.Text = "Начальная вершина:";
            // 
            // StartVertexUpDown
            // 
            this.StartVertexUpDown.Location = new System.Drawing.Point(123, 19);
            this.StartVertexUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.StartVertexUpDown.Name = "StartVertexUpDown";
            this.StartVertexUpDown.Size = new System.Drawing.Size(47, 20);
            this.StartVertexUpDown.TabIndex = 4;
            this.StartVertexUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // BFSradioBtn
            // 
            this.BFSradioBtn.AutoSize = true;
            this.BFSradioBtn.Location = new System.Drawing.Point(23, 83);
            this.BFSradioBtn.Name = "BFSradioBtn";
            this.BFSradioBtn.Size = new System.Drawing.Size(45, 17);
            this.BFSradioBtn.TabIndex = 3;
            this.BFSradioBtn.Text = "BFS";
            this.BFSradioBtn.UseVisualStyleBackColor = true;
            this.BFSradioBtn.CheckedChanged += new System.EventHandler(this.DFSradioBtn_CheckedChanged);
            // 
            // DFSradioBtn
            // 
            this.DFSradioBtn.AutoSize = true;
            this.DFSradioBtn.Location = new System.Drawing.Point(23, 63);
            this.DFSradioBtn.Name = "DFSradioBtn";
            this.DFSradioBtn.Size = new System.Drawing.Size(46, 17);
            this.DFSradioBtn.TabIndex = 2;
            this.DFSradioBtn.Text = "DFS";
            this.DFSradioBtn.UseVisualStyleBackColor = true;
            this.DFSradioBtn.CheckedChanged += new System.EventHandler(this.DFSradioBtn_CheckedChanged);
            // 
            // ShowHowItWorksBtn
            // 
            this.ShowHowItWorksBtn.Location = new System.Drawing.Point(41, 138);
            this.ShowHowItWorksBtn.Name = "ShowHowItWorksBtn";
            this.ShowHowItWorksBtn.Size = new System.Drawing.Size(111, 23);
            this.ShowHowItWorksBtn.TabIndex = 1;
            this.ShowHowItWorksBtn.Text = "Визуализировать";
            this.ShowHowItWorksBtn.UseVisualStyleBackColor = true;
            this.ShowHowItWorksBtn.Click += new System.EventHandler(this.ShowHowItWorksBtn_Click);
            // 
            // ResetBtn
            // 
            this.ResetBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ResetBtn.Enabled = false;
            this.ResetBtn.Location = new System.Drawing.Point(10, 285);
            this.ResetBtn.Name = "ResetBtn";
            this.ResetBtn.Size = new System.Drawing.Size(127, 23);
            this.ResetBtn.TabIndex = 4;
            this.ResetBtn.Text = "Сбросить/обновить";
            this.ResetBtn.UseVisualStyleBackColor = true;
            this.ResetBtn.Click += new System.EventHandler(this.ResetBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 319);
            this.Controls.Add(this.ResetBtn);
            this.Controls.Add(this.GroupOfManagement);
            this.Controls.Add(this.RunCodeBtn);
            this.Controls.Add(this.CodeTextArea);
            this.Icon = Resources.Icon;
            this.MinimumSize = new System.Drawing.Size(359, 357);
            this.Name = "MainForm";
            this.Text = "Управление графами";
            this.GroupOfManagement.ResumeLayout(false);
            this.GroupOfManagement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DelayTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhatLookingForUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartVertexUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox CodeTextArea;
        private System.Windows.Forms.Button RunCodeBtn;
        private System.Windows.Forms.GroupBox GroupOfManagement;
        private System.Windows.Forms.Label SetStartVertexLabel;
        private System.Windows.Forms.NumericUpDown StartVertexUpDown;
        private System.Windows.Forms.RadioButton BFSradioBtn;
        private System.Windows.Forms.RadioButton DFSradioBtn;
        private System.Windows.Forms.Button ShowHowItWorksBtn;
        private System.Windows.Forms.Button FindVertexBtn;
        private System.Windows.Forms.Label WhatLookingForLabel;
        private System.Windows.Forms.NumericUpDown WhatLookingForUpDown;
        private System.Windows.Forms.Button ResetBtn;
        private System.Windows.Forms.RadioButton DijkstraRadioBtn;
        private System.Windows.Forms.Label Distance;
        private System.Windows.Forms.Label CountOfSteps;
        private System.Windows.Forms.Label DistanceLabel;
        private System.Windows.Forms.Label CountOfStepsLabel;
        private System.Windows.Forms.RadioButton OriginalRadioBtn;
        private System.Windows.Forms.TrackBar DelayTrackBar;
        private System.Windows.Forms.Label DelayLabel;
    }
}