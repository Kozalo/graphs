﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Graphs.Properties;

namespace Graphs
{
    public class Graph
    {
        private List<Vertex> _vertices = new List<Vertex>();
        private int _iterator;
        private readonly Stack<int> _iteratorHistory = new Stack<int>();
        private GraphRenderer _renderer = null;
        private readonly Queue<Vertex> _vertexQueue = new Queue<Vertex>();

        public static int Steps = 0;
        public static int Distance = 0;

        public Graph()
        {
            _vertices.Add(new Vertex("1"));
            _iterator = 0;
        }

        public Graph(Graphs owner)
        {
            _vertices.Add(new Vertex("1"));
            _iterator = 0;
            _renderer = new GraphRenderer(owner, "1");
        }

        public void Root()
        {
            _iteratorHistory.Push(_iterator);
            _iterator = 0;
        }

        public void Back()
        {
            _iterator = _iteratorHistory.Count == 0 ? 0 : _iteratorHistory.Pop();
        }

        public int GetCountOfVertices() { return _vertices.Count; }

        public void AddVertex(int distance)
        {
            var newVertex = new Vertex((_vertices.Count + 1).ToString());
            _vertices.Add(newVertex);
            _vertices[_iterator].AddAdjacentVertex(newVertex, distance);
            newVertex.AddAdjacentVertex(_vertices[_iterator], distance);
            _renderer.DrawVertex(_iterator, _vertices.Count.ToString());
            _iteratorHistory.Push(_iterator);
            _iterator = _vertices.Count - 1;
        }

        public void JoinVertex(int number, int distance)
        {
            if ((number > 0) && (number <= _vertices.Count))
            {
                _vertices[_iterator].AddAdjacentVertex(_vertices[--number], distance);
                _vertices[number].AddAdjacentVertex(_vertices[_iterator], distance);
            }
            else
                Console.WriteLine(Resources.OutOfRange);
        }

        public void Close(int distance)
        {
            JoinVertex(1, distance);
            Root();
        }

        public override string ToString()
        {
            var resultStr = "[Graph] {\n";
            foreach (var vertex in _vertices)
                resultStr += $"\n   {vertex}\n";
            return resultStr + "\n}";
        }

        public FirstSearchResult Dfs(string whatLookingFor, int rootNumber = 1)
        {
            var fsr = _vertices[rootNumber - 1].Dfs(whatLookingFor);
            FS_Reset();
            return fsr;
        }

        public FirstSearchResult Bfs(string whatLookingFor, int rootNumber = 1)
        {
            _vertices[rootNumber - 1].DijkstraCost = 0;
            _vertexQueue.Enqueue(_vertices[rootNumber - 1]);
            while (_vertexQueue.Count != 0)
            {
                var currentVertex = _vertexQueue.Dequeue();
                if (currentVertex.GetContent() == whatLookingFor)
                {
                    var parent = currentVertex.Tag as Vertex;
                    var current = currentVertex;
                    while (parent != null)
                    {
                        Distance += current.GetDistanceBetween(parent);
                        current = parent;
                        parent = parent.Tag as Vertex;
                    }
                    
                    var fsr = new FirstSearchResult(currentVertex.GetId(), currentVertex.GetContent(), Steps, Distance);
                    FS_Reset();
                    return fsr;
                }
                currentVertex.MarkAs(VertexMark.Black);

                Vertex v;
                while ((v = currentVertex.GetWhiteVertex()) != null)
                {
                    v.MarkAs(VertexMark.Gray);
                    v.Tag = currentVertex;
                    _vertexQueue.Enqueue(v);
                    Steps++;
                }
            }

            FS_Reset();
            return null;
        }

        public FirstSearchResult Dijkstra(string whatLookingFor, int rootNumber = 1)
        {
            _vertices[rootNumber - 1].DijkstraCost = 0;
            _vertexQueue.Enqueue(_vertices[rootNumber - 1]);
            var resultId = -1;
            while (_vertexQueue.Count != 0)
            {
                var currentVertex = _vertexQueue.Dequeue();
                if (currentVertex.GetContent() == whatLookingFor)
                    resultId = currentVertex.GetId();

                Vertex v;
                while ((v = currentVertex.GetNonGrayVertex()) != null)
                {
                    v.MarkAs(VertexMark.Gray);
                    v.Tag = currentVertex;
                    _vertexQueue.Enqueue(v);
                    Steps++;
                }

                var forUnblock = new List<Vertex>();
                while ((v = currentVertex.GetNonBlackVertex()) != null)
                {
                    forUnblock.Add(v);
                    v.DijkstraBlocked = true;
                    int dist;
                    if ((dist = currentVertex.GetDistanceBetween(v) + currentVertex.DijkstraCost) < v.DijkstraCost)
                    {
                        v.DijkstraCost = dist;
                        v.Tag = currentVertex;
                    }
                    Steps++;
                }

                foreach (var vertex in forUnblock)
                    vertex.DijkstraBlocked = false;
            }

            var current = _vertices[resultId];
            var parent = _vertices[resultId].Tag as Vertex;
            var fsr = new FirstSearchResult(resultId, current.GetContent(), Steps, current.GetDistanceBetween(parent) + parent.DijkstraCost);
            FS_Reset();
            return fsr;
        }

        private void Bfs(PaintEventArgs e, GraphRenderer renderer, int rootNumber = 0, int sleeptime = 100)
        {
            _vertexQueue.Enqueue(_vertices[rootNumber]);
            while (_vertexQueue.Count != 0)
            {
                var currentVertex = _vertexQueue.Dequeue();
                currentVertex.MarkAs(VertexMark.Black);
                renderer.Label_ChangeColor(currentVertex.GetId(), Color.Black, Color.White);
                var lastPoint = renderer.Label_GetLocation(currentVertex.GetId());

                Vertex v;
                while ((v = currentVertex.GetWhiteVertex()) != null)
                {
                    v.MarkAs(VertexMark.Gray);
                    renderer.Label_ChangeColor(v.GetId(), Color.Gray, Color.Black);
                    System.Threading.Thread.Sleep(sleeptime);
                    e.Graphics.DrawLine(renderer.GetPen(), lastPoint, renderer.Label_GetLocation(v.GetId()));
                    _vertexQueue.Enqueue(v);
                }
            }

            FS_Reset();
        }

        private void FS_Reset()
        {
            foreach (var vertex in _vertices)
            {
                vertex.Reset();
                vertex.Tag = null;
                vertex.DijkstraCost = 999999;
            }
            _vertexQueue.Clear();
            Steps = 0;
            Distance = 0;
        }

        public string GetVertexContent(int number)
        {
            if ((number > 0) && (number <= _vertices.Count))
                return _vertices[number - 1].GetContent();
            else
            {
                Console.WriteLine(Resources.OutOfRange);
                return null;
            }
        }

        public void RewrawLines(PaintEventArgs e, RenderMode mode = RenderMode.Original, int vertexNumber = 0, int sleeptime = 1)
        {
            Reset();
            e.Graphics.Clear(SystemColors.Control);

            switch (mode) {
                case RenderMode.Original:
                    foreach (var v in _vertices)
                        v.ForEach(e, _renderer);
                    break;

                case RenderMode.Dfs:
                    _vertices[vertexNumber].DFS_Animation(e, _renderer, sleeptime * 100);
                    FS_Reset();
                    break;

                case RenderMode.Bfs:
                    Bfs(e, _renderer, vertexNumber, sleeptime * 100);
                    break;

                default:
                    MessageBox.Show(Resources.UnsupportedModeText, Resources.ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine(Resources.UnsupportedMode);
                    break;
            }
        }

        public void Reset()
        {
            foreach (var v in _vertices)
            {
                v.Reset();
                _renderer.Label_ChangeColor(v.GetId(), Color.Yellow, Color.Black);
            }
        }

        public void ParseCode(string code)
        {
            var strings = code.Split(';');
            var expr = new Regex(@"^\s*(AddVertex|Back|JoinVertex|Close|About|Anime)\(([0-9,\s]*)\)$", RegexOptions.Multiline | RegexOptions.IgnoreCase);

            foreach (var command in strings)
            {
                var parsedCode = expr.Split(command);
                if (parsedCode.Length < 4) continue;
                switch (parsedCode[1])
                {
                    case "AddVertex":
                        AddVertex(int.Parse(parsedCode[2]));
                        break;
                    case "Back":
                        Back();
                        break;
                    case "JoinVertex":
                        var parameters = parsedCode[2].Split(',');
                        JoinVertex(int.Parse(parameters[0]), int.Parse(parameters[1]));
                        break;
                    case "Close":
                        Close(int.Parse(parsedCode[2]));
                        break;
                    case "About":
                        if (MessageBox.Show(Resources.AboutText, Resources.AboutTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                            System.Diagnostics.Process.Start("http://kozalo.ru");
                        break;
                    case "Anime":
                        MessageBox.Show(Resources.AnimeText);
                        break;
                    default:
                        Console.WriteLine(Resources.UnknownCommand);
                        MessageBox.Show($"{Resources.UnknownCommandFoundText}: {parsedCode[1]}.", Resources.GraphParsingErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                }
            }
        }

        public void Dispose()
        {
            foreach (var v in _vertices)
                v.Dispose();
            _iterator = 0;
            _renderer.Dispose();
            _vertices = null;
            _renderer = null;
        }
    }

    public enum RenderMode { Original, Dfs, Bfs, Dijkstra };
}
