﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Graphs
{
    internal class GraphRenderer
    {
        private const int X = 300;
        private const int Y = 200;
        private const int Radius = 60;
        private const int Degrees = -60;

        private Graphs _owner;
        private List<Label> _vertexLabels = new List<Label>();
        private Pen _pen = new Pen(Color.Black, 3);
        private int _radius = Radius, _degrees = Degrees;

        public GraphRenderer(Graphs owner, string rootContent = "")
        {
            _owner = owner;

            _vertexLabels.Add(new Label());
            _vertexLabels[0].Location = new Point(X + (int)(_radius * Math.Cos(_degrees)), Y + (int)(_radius * Math.Sin(_degrees)));
            _vertexLabels[0].Text = rootContent;
            _vertexLabels[0].AutoSize = true;
            _owner.Controls.Add(_vertexLabels[0]);

            var rand = new Random(DateTime.Now.Millisecond);
            _radius += 10 + rand.Next(-5, 5);
            _degrees += 5 + rand.Next(-5, 5);
        }

        public void DrawVertex(int iterator, string content = "")
        {
            _vertexLabels.Add(new Label());
            var i = _vertexLabels.Count - 1;
            _vertexLabels[i].Location = new Point(X + (int)(_radius * Math.Cos(_degrees)), Y + (int)(_radius * Math.Sin(_degrees)));
            _vertexLabels[i].Text = content;
            _vertexLabels[i].AutoSize = true;
            _owner.Controls.Add(_vertexLabels[i]);

            var rand = new Random(DateTime.Now.Millisecond);
            _radius += 10 + rand.Next(-5, 5);
            _degrees += 5 + rand.Next(-5, 5);
        }

        public void Label_ChangeColor(int index, Color backColor, Color foreColor)
        {
            _vertexLabels[index].BackColor = backColor;
            _vertexLabels[index].ForeColor = foreColor;
        }

        public Point Label_GetLocation(int index)
        {
            return new Point(_vertexLabels[index].Location.X + (_vertexLabels[index].Width / 2), _vertexLabels[index].Location.Y + (_vertexLabels[index].Height / 2));
        }

        public Pen GetPen() { return _pen; }

        public void Dispose()
        {
            _owner = null;
            foreach (var l in _vertexLabels)
                l.Dispose();
            _vertexLabels = null;
            _pen.Dispose();
            _pen = null;
            _radius = Radius;
            _degrees = Degrees;
        }
    }
}
