﻿using Graphs.Properties;

namespace Graphs
{
    partial class Graphs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TestMsg = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TestMsg
            // 
            this.TestMsg.AutoSize = true;
            this.TestMsg.Location = new System.Drawing.Point(12, 9);
            this.TestMsg.Name = "TestMsg";
            this.TestMsg.Size = new System.Drawing.Size(148, 26);
            this.TestMsg.TabIndex = 0;
            this.TestMsg.Text = "If you see this text then\r\nthe program works incorrectly.";
            // 
            // Graphs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 569);
            this.Controls.Add(this.TestMsg);
            this.Icon = Resources.Icon;
            this.Name = "Graphs";
            this.Text = "Граф";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Graphs_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TestMsg;
    }
}

