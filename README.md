Graphs
======

My student work that uses Dijkstra's algorithm, breadth-first search, and depth-first search
to find the shortest path between two vertices of a graph.

Requirements: _.NET Framework 4.5_.

The author of the icon is [Nick Roach](http://www.elegantthemes.com).

Графы
=====

Моя студенческая работа, которая использует алгоритм Дейкстры, поиск в глубину и поиск в ширину
для поиска кратчайшего пути между двумя вершинами графа.

Требования: _.NET Framework 4.5_.

Автор иконки: [Nick Roach](http://www.elegantthemes.com).

Чуть более подробную инструкцию по языку описания графов можно прочитать [здесь](http://kozalo.ru/#post-1499129204).